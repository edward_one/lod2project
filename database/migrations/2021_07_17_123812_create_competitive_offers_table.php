<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitiveOffersTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'competitive_offers',
            function (Blueprint $table) {
                $table->id();
                $table->integer('universitySpecialitiesId')->comment('Код конкурсної пропозиції');
                $table->string('universitySpecialitiesName')->comment('Назва конкурсної пропозиції');
                $table->integer('qualification_level_id');
                $table->integer('faculty_id')->comment('ID факультета (у нашій БД)');
                $table->integer('speciality_id')->comment('ID спеціальності (у нашій БД)');
                $table->string('universitySpecialitiesTypeName')->comment("Фіксована, бюджетна, небюджетна");
                $table->string('educationFormName');
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('competitive_offers');
    }
}
