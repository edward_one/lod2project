<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProgramNamesColumnToCompetitiveOffersTable extends Migration
{
    public function up(): void
    {
        Schema::table(
            'competitive_offers',
            function (Blueprint $table) {
                $table->string('programNames')->nullable();
            }
        );
    }

    public function down(): void
    {
        Schema::table(
            'competitive_offers',
            function (Blueprint $table) {
                $table->dropColumn('programNames');
            }
        );
    }
}
