<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnglishMarkFromUniversityToEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::table(
            'enrollee_orders',
            function (Blueprint $table) {
                $table->float('english_mark_from_university')->default(0)
                    ->comment("Оцінка за іспит з англійської мови в університеті")
                    ->after('additional_mark');
            }
        );
    }

    public function down(): void
    {
        Schema::table(
            'enrollee_orders',
            function (Blueprint $table) {
                //
            }
        );
    }
}
