<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsForDocumentsInfoToEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::table('enrollee_orders', function (Blueprint $table) {
            $table->string('personDocumentType')->nullable();
            $table->string('documentSeries')->nullable();
            $table->string('documentNumber')->nullable();
            $table->timestamp('documentDateGet')->nullable();
            $table->string('documentIssued')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('enrollee_orders', function (Blueprint $table) {
            //
        });
    }
}
