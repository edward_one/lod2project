<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnForDodatokAndCheckboxToEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::table(
            'enrollee_orders',
            function (Blueprint $table) {
                $table->boolean('is_need_interview')->default(false);
                $table->float('interview_mark')->default(0);
                $table->string('path_to_addition')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'enrollee_orders',
            function (Blueprint $table) {
                //
            }
        );
    }
}
