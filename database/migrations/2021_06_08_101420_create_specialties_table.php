<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialtiesTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'specialties',
            function (Blueprint $table) {
                $table->id();
                $table->integer('faculty_id');
                $table->integer('edbo_speciality_id');
                $table->string('title');
                $table->string('code')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('specialties');
    }
}
