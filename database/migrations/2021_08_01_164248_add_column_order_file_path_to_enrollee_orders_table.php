<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOrderFilePathToEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::table('enrollee_orders', function (Blueprint $table) {
            $table->string('order_file_path')
                ->nullable()
                ->after('path_to_photo');
        });
    }

    public function down(): void
    {
        Schema::table('enrollee_orders', function (Blueprint $table) {
            //
        });
    }
}
