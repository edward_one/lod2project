<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('orderOfEnrollmentId');
            $table->string('orderOfEnrollmentNumber');
            $table->string('orderOfEnrollmentDate');
            $table->string('qualificationGroupName');
            $table->string('educationBaseName');
            $table->string('educationFormName');
            $table->string('courseName');
            $table->string('personEducationPaymentTypeName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
