<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacultiesTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'faculties',
            function (Blueprint $table) {
                $table->id();
                $table->integer('edbo_faculty_id');
                $table->string('title');
                $table->string('short_title')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('faculties');
    }
}
