<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameAndAddNewColumnForDiplomsMarkToEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::table(
            'enrollee_orders',
            function (Blueprint $table) {
                $table->renameColumn('diploma_mark', 'average_addition_diploma_mark');
            }
        );
    }
}
