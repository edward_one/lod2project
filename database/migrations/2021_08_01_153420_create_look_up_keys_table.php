<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookUpKeysTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'look_up_keys',
            function (Blueprint $table) {
                $table->id();
                $table->string('cls')->nullable();
                $table->integer('lk_id')->nullable();
                $table->string('name')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('look_up_keys');
    }
}
