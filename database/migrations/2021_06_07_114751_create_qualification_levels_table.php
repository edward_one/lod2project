<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQualificationLevelsTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'qualification_levels',
            function (Blueprint $table) {
                $table->id();
                $table->integer('edbo_qualification_level_id');
                $table->string('title');
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('qualification_levels');
    }
}
