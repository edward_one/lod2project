<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecializationsTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'specializations',
            function (Blueprint $table) {
                $table->id();
                $table->integer('edbo_specialization_id');
                $table->string('title');
                $table->string('code')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('specializations');
    }
}
