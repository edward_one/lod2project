<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnForDiplomsMarkToEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::table(
            'enrollee_orders',
            function (Blueprint $table) {
                $table->float('diploma_mark')->default(0);
            }
        );
    }
}
