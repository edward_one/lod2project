<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnrolleeOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::create(
            'enrollee_orders',
            function (Blueprint $table) {
                $table->id();
                $table->string('path_to_photo')->nullable();
                $table->string('full_name');
                $table->string('person_request_id')->nullable();
                $table->string('person_id')->nullable();
                $table->float('speciality_mark')->default(0);
                $table->float('english_mark')->default(0);
                $table->float('diploma_mark')->default(0);
                $table->float('rating')->default(0);
                $table->float('additional_mark')->default(0);
                $table->string('certificate_zno')->nullable();
                $table->enum('financing', ['budget', 'contract', 'budget_contract', 'additional'])
                    ->default('budget_contract');
                $table->integer('competitive_offer_id');
                $table->integer('qualification_level_id');
                $table->integer('diploma_id')->nullable();
                $table->string('personal_file_number')->comment('Номер особової справи')->nullable();
                $table->boolean('isOriginalDocumentsAdded')->default(false);
                $table->boolean('informationOriginalDocumentLocation')->default(false);
                $table->integer('education_budget')->default(1);
                $table->integer('status_id')->default(5);
                $table->integer('status_id_in_edbo')->default(5);
                $table->integer('order_id')->nullable();
                $table->boolean('is_ready_for_sync')->default(false);
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('enrollee_orders');
    }
}
