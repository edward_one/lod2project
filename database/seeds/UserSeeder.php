<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        User::firstOrCreate(
            [
                'email' => 'edward_admin@ukr.net',
            ],
            [
                'name' => 'Edward',
                'password' => bcrypt('123456!'),
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'ira_admin@ukr.net',
            ],
            [
                'name' => 'Іра',
                'password' => bcrypt('123456'),
            ]
        );

        User::firstOrCreate(
            [
                'email' => 'f_ksa@lod.vstup',
            ],
            [
                'name' => 'ФКСА',
                'password' => bcrypt('Sl0_97hdKl0N-152'),
                'faculty_id' => 5,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'f_btgp@lod.vstup',
            ],
            [
                'name' => 'ФБТЕГП',
                'password' => bcrypt('FeDc0NST-r4kt10_8625'),
                'faculty_id' => 3,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'f_eeem@lod.vstup',
            ],
            [
                'name' => 'ФЕЕЕМ',
                'password' => bcrypt('Tyth-7_U-fnjv_871'),
                'faculty_id' => 6,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'f_itki@lod.vstup',
            ],
            [
                'name' => 'ФІТКІ',
                'password' => bcrypt('Ghju_IPvsy91-SviT'),
                'faculty_id' => 7,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'f_mt@lod.vstup',
            ],
            [
                'name' => 'ФМТ',
                'password' => bcrypt('Ckfdf_Gf[fy_761-YtCGJHM'),
                'faculty_id' => 1,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'f_iren@lod.vstup',
            ],
            [
                'name' => 'ФІРЕН',
                'password' => bcrypt('NbMm-4JAhs_Osnsae828'),
                'faculty_id' => 4,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'f_mib@lod.vstup',
            ],
            [
                'name' => 'ФМІБ',
                'password' => bcrypt('Ie3Shu_76KloP_isaf'),
                'faculty_id' => 2,
            ]
        );
        User::firstOrCreate(
            [
                'email' => 'in_ebmd@lod.vstup',
            ],
            [
                'name' => 'ІнЕБМД',
                'password' => bcrypt('EeYsa62-newSHQ-isx'),
                'faculty_id' => 8,
            ]
        );
    }
}
