<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style type="text/css" media="all">
        * {
            font-family: DejaVu Sans;
            line-height: 15px;
        }

        .white-text {
            color: white;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid;
            text-align: center;
            font-size: 12pt;
            border-collapse: collapse;
        }

        table tr td {
            padding-top: 7px;
            padding-bottom: 7px;
        }

        .first-page {
            width: 100%;
        }

        .left-part {
            width: 45%;
            position: absolute;
            top: 0;
            left: 0;
        }

        table th {
            font-weight: 400;
        }

        .note {
            margin-top: 160px;
            text-align: justify;
        }

        .right-part {
            position: absolute;
            right: 0;
            top: 0;
            width: 45%;
        }

        .right-part .header {
            margin-left: 50%;
        }

        .right-part .form-number {
            margin-left: 65%;
            margin-top: 8px;
            font-weight: bold;
        }

        .right-part .form {
            margin-top: 10px;
            text-align: left;
        }

        .right-part .form .university {
            text-decoration: underline;
            width: 100%;
        }

        .right-part .form .subtext1 {
            font-size: 10px;
            text-align: center;
            margin-left: 20%;
            margin-top: -5px;
            display: inline-block;
        }

        .right-part .form .form-type {
            text-decoration: underline;
            width: 100%;
            margin-top: 10px;
        }

        .right-part .form .subtext2 {
            font-size: 10px;
            text-align: center;
            margin-left: 20%;
            display: inline-block;
        }

        .result {
            font-weight: bold;
            margin-top: 20px;
            text-align: center;
        }

        .result-number {
            font-weight: bold;
            margin-top: 5px;
            text-align: center;
        }

        .underline {
            text-decoration: underline;
        }

        .top-10 {
            margin-top: 10px;
        }

        .right-part .form .subtext3 {
            font-size: 10px;
            text-align: center;
            /*margin-left: 60%;*/
            /*display: inline-block;*/
            position: absolute;
            left: 150px;
        }

        .photo {
            margin-top: 60px;
        }

        .speciality {
            width: 60%;
            position: absolute;
            bottom: 110px;
            right: 0px;
            font-size: 12px;
        }

        .responsible1 {
            width: 60%;
            position: absolute;
            bottom: 30px;
            right: 0px;
            font-size: 12px;
        }

        .signature1 {
            width: 60%;
            position: absolute;
            bottom: -20px;
            right: 0px;
            margin-top: 50px;
        }

        .signature2 {
            width: 60%;
            position: absolute;
            bottom: 70px;
            right: 0px;
            margin-top: 50px;
            text-decoration: underline;
        }

        .right-part .form .subtext4 {
            position: absolute;
            font-size: 10px;
            bottom: -40px;
            right: 130px;
        }
        .right-part .form .MP {
            position: absolute;
            bottom: -80px;
            right: 245px;
        }
        .page-break {
            page-break-after: always;
        }
        .second-page .result-ocin {
            text-align: center;
            margin-top: 50px;
            margin-bottom: 20px;
        }
        .second-page .konkurs-bal {
            text-align: left;
            margin-top: 50px;
            margin-bottom: 20px;
        }
        .MP-2 {
            margin-top: 40px;
        }
        .signature3 {
            /*position: absolute;*/
            font-size: 10px;
            bottom: -20px;
            left: 730px;
        }
        .fio {
            /*position: absolute;*/
            font-size: 10px;
            bottom: -20px;
            left: 840px;
        }
    </style>
</head>
<body>
<div class="first-page">
    <div class="left-part">
        <table>
            <caption style="margin-bottom: 10px;">Розклад вступних випробувань</caption>
            <tr>
                <th>№ <br>
                    з/п
                </th>
                <th>Назва вступного <br> випробування</th>
                <th>Дата</th>
                <th>Час <br> початку</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Фахове вступне випробування</td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
            </tr>
            @if(!$enrollee->certificate_zno)
                <tr>
                    <td>2</td>
                    <td>Іноземна мова</td>
                    <td><span class="white-text">a</span></td>
                    <td><span class="white-text">a</span></td>
                </tr>
            @else
                <tr>
                    <td><span class="white-text">a</span></td>
                    <td><span class="white-text">a</span></td>
                    <td><span class="white-text">a</span></td>
                    <td><span class="white-text">a</span></td>
                </tr>
            @endif
            <tr>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
            </tr>
            <tr>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
            </tr>
            <tr>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
            </tr>
            <tr>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
                <td><span class="white-text">a</span></td>
            </tr>
        </table>
        <div class="note">
            <h3>ПРИМІТКИ:</h3>
            <p>1. Аркуш результатів вступних випробувань є перепусткою на випробування.</p>
            <p>2. Після закінчення кожного вступного випробування аркуш результатів вступних випробувань повинен бути
                повернений у приймальну комісію.</p>
            <p>3. При отриманні незадовільної оцінки аркуш результатів вступних випробувань залишається в
                екзаменатора.</p>
        </div>
    </div>

    <div class="right-part">
        <div class="header">
            <span>ЗАТВЕРДЖЕНО</span>
            <span>Наказ Міністерства освіти і</span>
            <span>науки України</span>
            <span><br>05 липня 2016 року № 782</span>
        </div>
        <div class="form-number">
            Форма № Н-1.04
        </div>
        <div class="form">
            <div class="university">
                Вінницький Національний Технічний Університет
                <span class="white-text">asdd</span>
            </div>
            <span class="subtext1">
                (найменування вищого навчального закладу)
            </span>

            <div class="form-type">
                Форма навчання: {{ \Illuminate\Support\Str::lower($enrollee->competitiveOffer->educationFormName) }}
                <span class="white-text">asddasddasddasddasddasddas</span>
            </div>
            <span class="subtext2">
                (денна, вечірня, заочна (дистанційна), екстернат)
            </span>

            <div class="result">
                АРКУШ РЕЗУЛЬТАТІВ ВСТУПНИХ ВИПРОБУВАНЬ
            </div>
            <div class="result-number">
                № <span class="underline"></span>{{ $enrollee->personal_file_number ??  "_______"}}
            </div>

            <div class="top-10">
                Прізвище: <span class="underline">{{ explode(' ', $enrollee->full_name)[0] }}</span>
            </div>
            <div class="top-10">
                Ім’я: <span class="underline">{{ explode(' ', $enrollee->full_name)[1] }}</span>
            </div>
            <div class="top-10">
                По батькові: <span class="underline">{{explode(' ', $enrollee->full_name)[2] }}</span>
            </div>
            <div class="top-10">
                Інститут, факультет, відділення: <span
                    class="underline">{{ $enrollee->competitiveOffer->faculty->short_title ?? "ФІТКІ"}}</span>
            </div>
            <div class="top-10">
                Рівень вищої освіти: <span class="underline">магістр</span>
            </div>
            <span class="subtext3">
                (бакалавр, магістр)
            </span>
            <img class="photo" src="{{ $image }}" alt="" width="150" height="200">

            <div class="speciality">
                {{ $enrollee->competitiveOffer->specialty->code }}
                {{ $enrollee->competitiveOffer->specialty->title }}
            </div>
            <span class="signature2">Ляховченко Н. В.</span>

            <div class="responsible1">
                Відповідальний секретар приймальної (відбіркової) комісії
            </div>
            <div class="signature1">
               ____________________________________
            </div>
            <div class="subtext4">
                (підпис)
            </div>
            <div class="MP">
                М.П.
            </div>
        </div>
    </div>
    <div class="page-break"></div>
</div>
<div class="second-page">
    <div class="result">
        АРКУШ РЕЗУЛЬТАТІВ ВСТУПНИХ ВИПРОБУВАНЬ
    </div>
    <div class="result-number">
        № <span class="underline"></span>{{ $enrollee->personal_file_number ??  "_______"}}
    </div>
    <div class="result-ocin">
        1. Результати оцінювання якості освіти та вступних екзаменів (творчих конкурсів)
    </div>
    <table>
        <tr>
            <th rowspan="2">№ <br>
                з/п
            </th>
            <th rowspan="2">Конкурсні предмети <br>
                (творчі конкурси)</th>
            <th rowspan="2">Сертифікат (номер), <br>
                атестат/диплом (серія, <br>
                номер) <br>
                або вид вступного <br>
                випробування (усне, <br>
                письмове тощо)</th>
            <th rowspan="2">Дата видачі <br>
                документа або дата <br>
                вступного <br>
                випробування <br>
                (число, місяць)</th>
            <th colspan="2">
                Кількість балів
            </th>
            <th rowspan="2">
                Прізвище, <br>
                ініціали та <br>
                підписи <br>
                екзаменаторів <br>
                (відповідальної <br>
                особи)
            </th>
        </tr>
        <tr>
            <th scope="col">Цифрами</th>
            <th scope="col">Словами</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Іноземна мова</td>
            <td>{{ $enrollee->certificate_zno ?? '' }}</td>
            <td>{{ $enrollee->subjectYear ?? '' }}</td>
            <td>{{ $enrollee->english_mark > 0 ? $enrollee->english_mark : '' }}</td>
            <td>{{ $enrollee->english_mark > 0 ? \App\Enums\NumberToWord::NUMBER_TO_WORD[$enrollee->english_mark] : ''}}</td>
            <td><span class="@if($enrollee->certificate_zno) white-text @endif">
                <span class="fio-name">Степанова І.С.</span><br>
                <span class="fio-name">Прадівлянний М.Г.</span><br>
                <span class="fio-name">Ібрагімова Л.В.</span><br>
            </span></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Фахове вступне випробування</td>
            <td><span class="white-text">a</span></td>
            <td><span class="white-text">a</span></td>
            <td><span class="white-text">a</span></td>
            <td><span class="white-text">a</span></td>
            <td>

                @foreach(explode(",", $enrollee->competitiveOffer->commissions) as $commision)
                    @if($commision)
                        <span class="fio-name">{{ $commision }}</span><br>
                    @endif
                @endforeach
                <span class="white-text">
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;<br>
                &nbsp;&nbsp;&nbsp;&nbsp;<br>
                </span>
            </td>
        </tr>
    </table>
    <div class="konkurs-bal">
        2. Конкурсний бал (сума балів)__________________________________
    </div>
    <div class="MP-2">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;М. П.
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Відповідальний секретар приймальної (відбіркової) комісії_____________ &nbsp;&nbsp;&nbsp; <span class="underline">Ляховченко Н. В.</span>
    </div>
    <div class="signature3">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        (підпис) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        (прізвище та ініціали)
    </div>
    <div class="fio">

    </div>
</div>
</body>
</html>
