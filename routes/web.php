<?php

use App\Jobs\SendInfoToEDBOJob;
use App\Jobs\SendMarksToEDBOJob;
use App\Jobs\SendStatusesToEDBOJob;
use App\Jobs\SyncEnrolleeOrdersJob;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Support\Facades\Route;

Route::get('/invite/{id}', 'EnrolleeOrderController@generateInvite');
Route::post('/import/csv', 'CSVImporterController@importFile');

Route::get(
    '/',
    function () {
        return redirect('/nova');
    }
);

Route::get(
    '/sync/{key}',
    static function ($key) {
        if ($key === "enrollee") {
            SyncEnrolleeOrdersJob::dispatch();

            return "enrollee";
        } elseif ($key === "info") {
            SendInfoToEDBOJob::dispatch();

            return "info";
        } elseif ($key === "marks") {
            SendMarksToEDBOJob::dispatch();

            return "Marks";
        } elseif ($key === "status") {
            SendStatusesToEDBOJob::dispatch();

            return "status";
        } elseif ($key === "order") {
            EDBOSyncService::syncOrdersFromEDBO();

            return "order";
        }

        return 'Hello!';
    }
);
