<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramMessage;

class EDBOReceivedDataNotification extends Notification
{
    use Queueable;

    public $id;
    public $countAll;
    public $countPhoto;
    public $countWithoutZNO;

    public function __construct(string $id, string $countAll, string $countPhoto, string $countWithoutZNO)
    {
        $this->id = $id;
        $this->countAll = $countAll;
        $this->countPhoto = $countPhoto;
        $this->countWithoutZNO = $countWithoutZNO;
    }

    public function via($notifiable): array
    {
        return ['telegram'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to(User::TELEGRAM_CHAT_ID)
            ->content(
                sprintf(
                    "
❗️Завдання номер %s❗️
Успішно стягнуто заяви з ЄДБО.

Всього опрацьовано заяв: %s ✅
Всього опрацьовано фото: %s ✅
Всього заяв з сертифікатом ЗНО: %s ✅
            ",
                    $this->id,
                    $this->countAll,
                    $this->countPhoto,
                    $this->countWithoutZNO,
                )
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
