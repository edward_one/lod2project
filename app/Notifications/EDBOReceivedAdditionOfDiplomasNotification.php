<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramMessage;

class EDBOReceivedAdditionOfDiplomasNotification extends Notification
{
    use Queueable;

    public $id;
    public $countAll;

    public function __construct(string $id, string $countAll)
    {
        $this->id = $id;
        $this->countAll = $countAll;
    }

    public function via($notifiable): array
    {
        return ['telegram'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to(User::TELEGRAM_CHAT_ID)
            ->content(
                sprintf(
                    "
❗️Завдання номер %s❗️
Успішно синхронізовано додатки до диплому з ЄДБО.

Всього заяв з додатками: %s ✅
            ",
                    $this->id,
                    $this->countAll,
                )
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
