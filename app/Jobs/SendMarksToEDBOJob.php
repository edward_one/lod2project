<?php

namespace App\Jobs;

use App\Models\EnrolleeOrder;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMarksToEDBOJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct()
    {
        //
    }

    public function handle()
    {
        $enrolls = EnrolleeOrder::all();

        foreach ($enrolls as $enrollee) {
            EDBOSyncService::sendMarksToEDBOForEnrollee($enrollee);
        }
    }
}
