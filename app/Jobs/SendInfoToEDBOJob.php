<?php

namespace App\Jobs;

use App\Models\EnrolleeOrder;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendInfoToEDBOJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct()
    {
    }

    public function handle(): void
    {
        $enrolls = EnrolleeOrder::all();

        foreach ($enrolls as $enrollee) {
            EDBOSyncService::sendInfoForEnrollee($enrollee);
        }
    }
}
