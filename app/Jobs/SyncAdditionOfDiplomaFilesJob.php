<?php

namespace App\Jobs;

use App\Models\EnrolleeOrder;
use App\Notifications\EDBOReceivedAdditionOfDiplomasNotification;
use App\Services\EDBO\EDBOSyncService;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncAdditionOfDiplomaFilesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function handle(): void
    {
        $service = new EDBOSyncService();
        $service->syncAdditionOfDiplomas();

        $user = User::query()->first();

        $user->notify(
            new EDBOReceivedAdditionOfDiplomasNotification(
                $this->id,
                EnrolleeOrder::whereNotNull('path_to_addition')->count(),
            )
        );
    }
}
