<?php

namespace App\Jobs;

use App\Models\EnrolleeOrder;
use App\Notifications\EDBOReceivedDataNotification;
use App\Services\EDBO\EDBOSyncService;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class SyncEnrolleeOrdersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle(): void
    {
        $service = new EDBOSyncService();
        $service->syncCompetitiveOffers();
        $service->syncEnrolleeOrders();
        $service->syncEnrolleeEnglishMarkZNO();
        $service->syncPhotos();

        $user = User::first();

        $id = Cache::increment('custom_id');
        $user->notify(
            new EDBOReceivedDataNotification(
                $id,
                EnrolleeOrder::count(),
                EnrolleeOrder::whereNotNull('path_to_photo')->count(),
                EnrolleeOrder::whereNotNull('certificate_zno')->count()
            )
        );

        SyncAdditionOfDiplomaFilesJob::dispatch($id);
    }
}
