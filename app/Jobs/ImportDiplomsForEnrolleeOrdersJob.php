<?php

namespace App\Jobs;

use App\Models\EnrolleeOrder;
use App\Services\LOD\LODSyncService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ImportDiplomsForEnrolleeOrdersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function handle(): void
    {
        $fileData = $this->readCSV(
            'app/public/csv/'.$this->fileName,
            [
                'delimiter' => ','
            ]
        );

        $counter = 0;
        foreach ($fileData as $row) {
            if ($counter === 0) {
                $counter++;
                continue;
            }
            Log::alert('Here - '.$row[1].' '.$row[2]);
            if (is_null($row) || !isset($row[1])) {
                break;
            }

            EnrolleeOrder::query()
                ->where('person_id', $row[1])
                ->update(
                    [
                        'personDocumentType' => $row[2],
                        'documentSeries' => $row[3],
                        'documentNumber' => $row[4],
                        'documentDateGet' => $row[5],
                        'documentIssued' => $row[6],
                    ]
                );
        }

        $service = new LODSyncService();
        $service->syncDiplomaMarks();
    }

    public function readCSV($csvFile, $array): array
    {
        $file_handle = fopen(storage_path($csvFile), 'r');
        $line_of_text = [];
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }
}
