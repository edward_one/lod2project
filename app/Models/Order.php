<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    public function enrolleeOrder()
    {
        return $this->hasOne(EnrolleeOrder::class);
    }
}
