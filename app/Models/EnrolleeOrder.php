<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int id
 * @property string path_to_photo
 * @property string full_name
 * @property string person_request_id
 * @property string person_id
 * @property float speciality_mark
 * @property float rating
 * @property float additional_mark
 * @property null|string certificate_zno
 * @property string financing
 * @property int competitive_offer_id
 * @property int qualification_level_id
 * @property null|string personal_file_number
 * @property int status_id
 *
 * @see EnrolleeOrder::competitiveOffer()
 * @property CompetitiveOffer competitiveOffer
 */
class EnrolleeOrder extends Model
{
    protected $guarded = ['id'];

    public function qualificationLevel()
    {
        return $this->belongsTo(QualificationLevel::class);
    }

    public function competitiveOffer(): BelongsTo
    {
        return $this->belongsTo(CompetitiveOffer::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
