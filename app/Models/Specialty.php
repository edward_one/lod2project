<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    protected $fillable = [
        'faculty_id',
        'edbo_speciality_id',
        'title',
        'code',
    ];

    public function getTitle(): string
    {
        return $this->getAttribute('title');
    }

    public function getCode(): string
    {
        return $this->getAttribute('code');
    }

    public function getFacultyId(): int
    {
        return $this->getAttribute('faculty_id');
    }
}
