<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LookUpKeys extends Model
{
    protected $guarded = ['id'];
}
