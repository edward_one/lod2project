<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int edbo_specialization_id
 * @property string title
 * @property string code
 */
class Specialization extends Model
{
    protected $fillable = [
        'edbo_specialization_id',
        'title',
        'code'
    ];
}
