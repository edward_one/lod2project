<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int universitySpecialitiesId
 * @property string universitySpecialitiesName
 * @property int qualification_level_id
 * @property int faculty_id
 * @property int speciality_id
 * @property string universitySpecialitiesTypeName
 * @property string educationFormName
 */
class CompetitiveOffer extends Model
{
    protected $fillable = [
        'universitySpecialitiesId',
        'universitySpecialitiesName',
        'qualification_level_id',
        'faculty_id',
        'speciality_id',
        'universitySpecialitiesTypeName',
        'educationFormName',
        'programNames',
        'commissions'
    ];

    public function faculty()
    {
        return $this->belongsTo(Faculty::class);
    }

    public function specialty()
    {
        return $this->belongsTo(Specialty::class, 'speciality_id');
    }

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            CompetitiveOffer::where('programNames', $model->programNames)->update(['commissions' => $model->commissions]);
        });
        self::updated(function($model){
            CompetitiveOffer::where('programNames', $model->programNames)->update(['commissions' => $model->commissions]);
        });
    }
}
