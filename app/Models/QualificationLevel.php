<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int edbo_qualification_level_id
 * @property string title
 */
class QualificationLevel extends Model
{
    protected $fillable = [
        'edbo_qualification_level_id',
        'title'
    ];
}
