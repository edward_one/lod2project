<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 * @property int edbo_faculty_id
 * @property string title
 */
class Faculty extends Model
{
    protected $fillable = [
        'edbo_faculty_id',
        'title'
    ];

    public function specialities(): HasMany
    {
        return $this->hasMany(Specialty::class);
    }
}
