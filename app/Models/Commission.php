<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    protected $table = 'comissions';
    protected $guarded = ['id'];
    protected $fillable = ['title'];

    public function competitiveOffer()
    {
        return $this->belongsTo(CompetitiveOffer::class, 'competitive_offer_id');
    }
}
