<?php

namespace App\Services\EDBO;

use Cache;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use JsonException;

class EDBOApiService
{
    private Client $client;
    private string $url;
    private string $suffix = '/data/EDEBOWebApi/';

    private function __construct()
    {
        $this->client = new Client();
        $this->url = getenv('EDBO_HOST').':'.getenv('EDBO_PORT').$this->suffix;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public static function postJSON(string $prefix = '', array $data = [], array $headers = []): array
    {
        $instance = new static();

        $authorizationHeader = [];
        if ($prefix !== "oauth/token") {
            $token = static::getAccessToken();
            $authorizationHeader = [
                'Accept' => '/*/',
                'authorization' => 'Bearer '.$token
            ];
        }

        $payload = [
            'headers' => array_merge($authorizationHeader, $headers),
            'json' => $data
        ];

        $response = null;

        try {
            $response = $instance->client->post($instance->url.$prefix, $payload);
        } catch (ServerException | ClientException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();

            Log::channel('send_to_edbo_info')
                ->error("Ошибка при запросе в EDBO(post) ID: ".$response, $data);
            throw $exception;
        } catch (Exception $exception) {
            throw $exception;
        }

        if (Str::startsWith($prefix, "api/entrance/files/") || Str::startsWith(
                $prefix,
                "api/entrance/personRequest/reports/personRequest"
            )) {
            return [
                'file_content' => $response->getBody()->getContents(),
                'extension' => explode('/', $response->getHeader('Content-Type')[0])[1]
            ];
        }

        $response = $response->getBody()->getContents();

        if ($response == 'true') {
            return ['status' => true];
        }

        if ($response == 'false') {
            return ['status' => false];
        }

        if (is_float($response)) {
            return ['result' => $response];
        }

        return ['result' => $response];
        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    public static function getAccessToken(): string
    {
        return Cache::remember(
            "EDBOAccessToken",
            1600,
            function () {
                return static::auth()['access_token'];
            }
        );
    }

    public static function auth()
    {
        $data = [
            'username' => 'perepelytsia.viacheslav@edbo.gov.ua',
            'password' => 'Challenge_1992_bellsmsxl98',
            'grant_type' => 'password',
            'app_key' => getenv('EDBO_APP_KEY'),
        ];

        return static::post('oauth/token', $data);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     * @throws Exception
     */
    public static function post(string $prefix = '', array $data = [], array $headers = []): array
    {
        $instance = new static();

        $authorizationHeader = [];
        if ($prefix !== "oauth/token") {
            $token = static::getAccessToken();
            $authorizationHeader = [
                'Accept' => '/*/',
                'authorization' => 'Bearer '.$token
            ];
        }

        $payload = [
            'headers' => array_merge($authorizationHeader, $headers),
            'form_params' => $data
        ];

        $response = null;

        try {
            $response = $instance->client->post($instance->url.$prefix, $payload);
        } catch (ServerException | ClientException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();

            Log::channel('send_to_edbo_info')
                ->error("Ошибка при запросе в EDBO(post) ID: ".$response, $data);
            throw $exception;
        } catch (Exception $exception) {
            throw $exception;
        }

        if (Str::startsWith($prefix, "api/entrance/files/") || Str::startsWith(
                $prefix,
                "api/entrance/personRequest/reports/personRequest"
            )) {
            return [
                'file_content' => $response->getBody()->getContents(),
                'extension' => explode('/', $response->getHeader('Content-Type')[0])[1]
            ];
        }

        $response = $response->getBody()->getContents();

        if ($response == 'true') {
            return ['status' => true];
        }

        if ($response == 'false') {
            return ['status' => false];
        }

        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    public static function getAllCompetitiveOffersForMaster(): array
    {
        $response = static::fetchCompetitiveOffers();

        return array_filter(
            $response,
            static function ($data) {
                $isMaster = $data['qualificationGroupName'] === 'Магістр';
                $startDateYear = Carbon::parse("2021-09-01", "+03:00");

                return $isMaster && Carbon::parse($data['educationDateBegin'])->gte($startDateYear);
            }
        );
    }

    public static function fetchCompetitiveOffers(): array
    {
        return static::post(
            'api/entrance/specialities/list',
            [
                'universityID' => 137,
                'pageSize' => 500
            ]
        );
    }

    public static function fetchSpecializations(): array
    {
        return static::post(
            'api/universitySpecializations/list',
            [
                'universityID' => 137,
                'pageSize' => 1000
            ]
        );
    }

    public static function fetchSpecialties(): array
    {
        return static::post(
            'api/universitySpecializations/list',
            [
                'universityID' => 137,
                'pageSize' => 1000
            ]
        );
    }

    public static function fetchOrders(int $universitySpecialitiesId): array
    {
        $limit = 100;

        $result = static::post(
            'api/entrance/personRequest/list2',
            [
                'universityID' => 137,
                'universitySpecialitiesId' => $universitySpecialitiesId,
                'p_PageNo' => 0,
                'p_pageSize' => $limit,
                'statusesList' => [
                    ['id' => 2],
                    ['id' => 3],
                    ['id' => 4],
                    ['id' => 5],
                    ['id' => 6],
                    ['id' => 7],
                    ['id' => 8],
                    ['id' => 9],
                    ['id' => 10],
                    ['id' => 11],
                    ['id' => 12],
                    ['id' => 13],
                    ['id' => 14],
                    ['id' => 15],
                ],
            ]
        );

        if (count($result) === $limit) {
            $result = array_merge(
                $result,
                static::post(
                    'api/entrance/personRequest/list2',
                    [
                        'universityID' => 137,
                        'universitySpecialitiesId' => $universitySpecialitiesId,
                        'p_PageNo' => 1,
                        'p_pageSize' => $limit,
                        'statusesList' => [
                            ['id' => 2],
                            ['id' => 3],
                            ['id' => 4],
                            ['id' => 5],
                            ['id' => 6],
                            ['id' => 7],
                            ['id' => 8],
                            ['id' => 9],
                            ['id' => 10],
                            ['id' => 11],
                            ['id' => 12],
                            ['id' => 13],
                            ['id' => 14],
                            ['id' => 15],
                        ],
                    ]
                )
            );
        }

        return $result;
    }

    public static function fetchPhotoByPersonRequestId($personRequestId): array
    {
        return static::post(
            'api/entrance/files/photo',
            [
                'personRequestId' => $personRequestId
            ]
        );
    }

    public static function fetchEDBOOrderByPersonRequestId($personRequestId): array
    {
        return static::postJSON(
            'api/entrance/personRequest/reports/personRequest',
            [
                'personRequestId' => $personRequestId
            ]
        );
    }

    public static function fetchAdditionOfDiplomaPhotoByPersonRequestId($personRequestId): array
    {
        return static::post(
            'api/entrance/files/dodatok',
            [
                'personRequestId' => $personRequestId
            ]
        );
    }

    public static function updateSubjectResultForPerson(array $args = []): array
    {
        return static::post('api/entrance/personRequest/subjectResult/update', $args);
    }

    public static function getSubjectList($universitySpecialitiesId): array
    {
        return static::post(
            'api/entrance/specialities/entrysubject/list',
            [
                'UniversitySpecialitiesId' => $universitySpecialitiesId
            ]
        );
    }

    public static function getInformationAboutCertificateZNO($personId): array
    {
        return static::post(
            'api/entrance/personRequest/certificateZNO/list',
            [
                'personId' => $personId
            ]
        );
    }
}
