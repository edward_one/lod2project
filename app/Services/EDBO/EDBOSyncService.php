<?php

namespace App\Services\EDBO;

use App\Models\CompetitiveOffer;
use App\Models\EnrolleeOrder;
use App\Models\Faculty;
use App\Models\Order;
use App\Models\QualificationLevel;
use App\Models\Specialization;
use App\Models\Specialty;
use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;

class EDBOSyncService
{
    protected array $competitiveOffers;

    public static function sendMarksToEDBOForEnrollee(EnrolleeOrder $enrolleeOrder): array
    {
        try {
            $response = EDBOApiService::postJSON(
                'api/entrance/personRequest/subjectResult/update',
                [
                    'PersonRequestId' => $enrolleeOrder->person_request_id,
                    'subjectResult' => static::getSubjectResultsForEnrollee($enrolleeOrder),
                ]
            );
        } catch (ServerException | ClientException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();

            Log::channel('send_to_edbo_info')
                ->error("Ошибка при синхронизации оценок для ID = : " . $enrolleeOrder->id . ' Причина: ' . $response);

            return [];
        } catch (Exception $exception) {
            Log::channel('send_to_edbo_info')
                ->error(
                    "Ошибка при синхронизации оценок(2) для ID = : " . $enrolleeOrder->id . ' Причина: ' . $exception->getMessage(
                    )
                );
            return [];
        }

        return $response;
    }

    public static function getSubjectResultsForEnrollee(EnrolleeOrder $enrolleeOrder): array
    {
        $subjects = EDBOApiService::post(
            'api/entrance/specialities/entrysubject/list',
            [
                'universitySpecialitiesId' => $enrolleeOrder->competitiveOffer->universitySpecialitiesId
            ]
        );

        $response = [];

        foreach ($subjects as $subject) {
            $subjectName = Str::lower($subject['subjectName']);
            $examFormName = Str::lower($subject['examFormName']);

            if (Str::contains($subjectName, Str::lower('Іноземна мова'))) {
                $ZNOs = EDBOApiService::post(
                    'api/entrance/personRequest/ZNOTechnology/list',
                    [
                        'personId' => $enrolleeOrder->person_id
                    ]
                );
                if (count($ZNOs) > 0) {
                    foreach ($ZNOs as $ZNO) {
                        $subjectName = Str::lower($ZNO['subjectName']);
                        if ($ZNO['znoTechnologySubjectId'] && Str::contains(
                                $subjectName,
                                'мова'
                            ) && $ZNO['subjectValue'] >= 100) {
                            $response[] = [
                                'universitySpecialitySubjectId' => $subject['universitySpecialitySubjectId'],
                                'mainScore' => $enrolleeOrder->english_mark,
                                'ZNOTechnologySubjectId' => $ZNO['znoTechnologySubjectId']
                            ];
                        } else {
                            $response[] = [
                                'universitySpecialitySubjectId' => $subject['universitySpecialitySubjectId'],
                                'mainScore' => $enrolleeOrder->english_mark_from_university,
                            ];
                        }
                    }
                } else {
                    $response[] = [
                        'universitySpecialitySubjectId' => $subject['universitySpecialitySubjectId'],
                        'mainScore' => $enrolleeOrder->english_mark_from_university,
                    ];
                }
            } elseif (Str::contains($subjectName, Str::lower('Інші показники конкурсного відбору'))) {
                $response[] = [
                    'universitySpecialitySubjectId' => $subject['universitySpecialitySubjectId'],
                    'mainScore' => $enrolleeOrder->additional_mark,
                ];
            } elseif (
                (
                    Str::contains($subjectName, Str::lower('Вступне')) &&
                    Str::contains($subjectName, Str::lower('фахове')) &&
                    Str::contains($subjectName, Str::lower('випробовування')) &&
                    !Str::contains($examFormName, Str::lower('залік'))
                )
                ||
                (
                Str::contains($examFormName, Str::lower('Фахове випробування'))
                )
            ) {
                $response[] = [
                    'universitySpecialitySubjectId' => $subject['universitySpecialitySubjectId'],
                    'mainScore' => $enrolleeOrder->speciality_mark,
                ];
            } elseif (Str::contains($subjectName, Str::lower('Додаткове вступне випробовування'))) {
                if ($enrolleeOrder->is_need_interview) {
                    $response[] = [
                        'universitySpecialitySubjectId' => $subject['universitySpecialitySubjectId'],
                        'mainScore' => $enrolleeOrder->interview_mark,
                    ];
                }
            }
        }

        return $response;
    }

    public static function updateStatusForEnrollee(EnrolleeOrder $enrolleeOrder): array
    {
        $configEnrollLevel = [
            'budget_contract' => 2,
            'budget' => 1,
            'contract' => 0,
        ];
        $requestData = [
            'personRequestList' => [
                ['id' => $enrolleeOrder->person_request_id]
            ],
            'personRequestStatusTypeId' => $enrolleeOrder->status_id,
            'enrollLevel' => $configEnrollLevel[$enrolleeOrder->financing],
        ];

        if ($enrolleeOrder->status_id === 7) {
            $requestData = array_merge($requestData, ['denyDescription' => "Обрав іншу спеціальність"]);
        }

        if ($enrolleeOrder->status_id === 9) {
            $requestData = array_merge($requestData, ['budgetRecommendationTypeId' => 1]);
        }

        if ($enrolleeOrder->status_id === 10) {
            $requestData = array_merge(
                $requestData,
                ['budgetRejectDescription' => 'Абітурієнт не приніс оригінали документів.']
            );
        }

        if ($enrolleeOrder->status_id === 12) {
            $requestData = array_merge($requestData, ['contractRecommendationTypeId' => 1]);
        }

        if ($enrolleeOrder->status_id === 13) {
            $requestData = array_merge(
                $requestData,
                ['contractRejectDescription' => 'Абітурієнт не приніс оригінали документів.']
            );
        }

        if ($enrolleeOrder->status_id === 14) {
            $requestData = array_merge(
                $requestData,
                ['orderOfEnrollmentId' => (int)$enrolleeOrder->order->orderOfEnrollmentId]
            );
        }

        try {
            $result = EDBOApiService::post(
                'api/entrance/personRequest/changeStatus',
                $requestData
            );

            $enrolleeOrder->update(['is_ready_for_sync' => false]);
        } catch (ServerException | ClientException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();

            Log::channel('send_to_edbo_info')
                ->error(
                    "Ошибка при синхронизации статусов для ID = : " . $enrolleeOrder->id . ' Причина: ' . $response
                );

            return [];
        } catch (Exception $exception) {
            Log::channel('send_to_edbo_info')
                ->error(
                    "Ошибка при синхронизации статусов(2) для ID = : " . $enrolleeOrder->id . ' Причина: ' . $exception->getMessage(
                    )
                );
            return [];
        }

        return $result;
    }

    public static function sendInfoForEnrollee(EnrolleeOrder $enrollee): array
    {
        $response = static::fetchInfoForEnrollee($enrollee);

        if (!isset($response) || count($response) < 1) {
            return [];
        }

        $data = [
            'personRequestId' => (int)$enrollee->person_request_id,
            'isGraduateOneYear' => $response['isGraduateOneYear'],
            'isAdditionalExam' => $enrollee->is_need_interview,
            'isClaimForBudget' => $enrollee->financing === 'budget' || $enrollee->financing === 'budget_contract',
            'isClaimForContract' => $enrollee->financing === 'contract' || $enrollee->financing === 'budget_contract',
            'isBudgetEducation' => $enrollee->education_budget,
            'isAnotherBudgetAllowed' => $response['isAnotherBudgetAllowed'],
            'isSimiliarSpeciality' => $response['isSimiliarSpeciality'],
            'isInterviewSuccess' => $enrollee->interview_mark > 111.0,
            'isOriginalDocumentsAdded' => $enrollee->isOriginalDocumentsAdded,
            'informationOriginalDocumentLocation' => $enrollee->informationOriginalDocumentLocation,
            'PersonalCode' => $enrollee->personal_file_number,
            'IsConfirmedContract' => $response['isConfirmedContract'],
        ];

        $response = [];

        try {
            $response = EDBOApiService::postJSON('api/entrance/personRequest/update', $data);
//            $enrollee->update(['is_ready_for_sync' => false]);
        } catch (ServerException | ClientException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();

            Log::channel('send_to_edbo_info')->info(
                'Enrollee ID (guzzle) = ' . $enrollee->id,
                [
                    'exception' => $response
                ]
            );
            return [];
        } catch (GuzzleException $e) {
            Log::channel('send_to_edbo_info')->info(
                'Enrollee ID (guzzle) = ' . $enrollee->id,
                [
                    'exception' => $e->getMessage()
                ]
            );
        } catch (Exception $exception) {
            Log::channel('send_to_edbo_info')->info(
                'Enrollee ID = ' . $enrollee->id,
                [
                    'exception' => $exception->getMessage()
                ]
            );
        }

        return $response;
    }

    public static function fetchInfoForEnrollee(EnrolleeOrder $enrolleeOrder)
    {
        $response = [];

        try {
            $response = EDBOApiService::postJSON(
                'api/entrance/personRequest/list2',
                [
                    'universitySpecialitiesId' => (int)$enrolleeOrder->competitiveOffer->universitySpecialitiesId,
                    'personId' => (int)$enrolleeOrder->person_id,
                    'p_PageNo' => 0,
                    'p_PageSize' => 50,
                    'statusesList' => [
                        ['id' => 5],
                        ['id' => 6],
                    ],
                ]
            );
        } catch (ServerException | ClientException $exception) {
            Log::channel('send_to_edbo_info')
                ->error("Проблема с ID: " . $enrolleeOrder->id);
        }

        try {
            return $response[0];
        } catch (Exception $exception) {
            Log::channel('send_to_edbo_info')
                ->error('Проблема: ' . $enrolleeOrder->id, $response);
        }
    }

    public static function syncTypesKonkurs()
    {
        $data = [
            'lookupKey' => 'Common_RecommendationTypes',
            'universityCode' => '99dc6043-f9df-4c77-b545-d3b0bd80cc75',
            'universityId' => 137,
        ];

        try {
            $response = EDBOApiService::post('api/dictionary/get', $data);
            dd($response);
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    public static function syncEDBOOrderForEnrollee(EnrolleeOrder $enrollee): void
    {
        $pathForPhotos = 'orders/';

        try {
            $response = EDBOApiService::fetchEDBOOrderByPersonRequestId($enrollee->person_request_id);
            $fileContent = $response['file_content'];
            $extension = $response['extension'];
            $fullPath = $pathForPhotos . $enrollee->person_request_id . '.' . $extension;
            $enrollee->order_file_path = $fullPath;
            $enrollee->save();
            if (!Storage::disk('public')->exists($fullPath)) {
                Storage::disk('public')
                    ->put($fullPath, $fileContent);
            }
            usleep(300000);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    public static function syncOrdersFromEDBO(): void
    {
        $data = [
            'receptionSeasonId' => 10,
            'universityId' => 137,
            'pageNo' => 0
        ];

        $response = EDBOApiService::post('api/entrance/enrollOrder/list', $data);

        foreach ($response as $data) {
            Order::query()->firstOrCreate(
                [
                    'orderOfEnrollmentId' => $data['orderOfEnrollmentId'],
                    'orderOfEnrollmentNumber' => $data['orderOfEnrollmentNumber'],
                    'orderOfEnrollmentDate' => $data['orderOfEnrollmentDate'],
                    'qualificationGroupName' => $data['qualificationGroupName'],
                    'educationBaseName' => $data['educationBaseName'],
                    'educationFormName' => $data['educationFormName'],
                    'courseName' => $data['courseName'],
                    'personEducationPaymentTypeName' => $data['personEducationPaymentTypeName'],
                ]
            );
        }
    }

    public function syncPhotos(): void
    {
        $enrolleeOrders = EnrolleeOrder::query()->whereNull('path_to_photo')->get();
        $pathForPhotos = 'documents/';

        foreach ($enrolleeOrders as $enrolleeOrder) {
            try {
                $response = EDBOApiService::fetchPhotoByPersonRequestId($enrolleeOrder->person_request_id);
                $fileContent = $response['file_content'];
                $extension = $response['extension'];
                $fullPath = $pathForPhotos . $enrolleeOrder->person_request_id . '.' . $extension;
                $enrolleeOrder->path_to_photo = $fullPath;
                $enrolleeOrder->save();
                if (!Storage::disk('public')->exists($fullPath)) {
                    Storage::disk('public')
                        ->put($fullPath, $fileContent);
                }
                usleep(300000);
            } catch (Throwable $exception) {
                Log::error("Не удалось получить фото для person_request_id = " . $enrolleeOrder->person_request_id);
            }
        }
    }

    public function syncAdditionOfDiplomas(): void
    {
        $enrolleeOrders = EnrolleeOrder::query()->whereNull('path_to_addition')->get();
        $pathForPhotos = 'additionOfDiplomas/';

        foreach ($enrolleeOrders as $enrolleeOrder) {
            try {
                $response = EDBOApiService::fetchAdditionOfDiplomaPhotoByPersonRequestId(
                    $enrolleeOrder->person_request_id
                );
                $fileContent = $response['file_content'];
                $extension = $response['extension'];
                $fullPath = $pathForPhotos . $enrolleeOrder->person_request_id . '.' . $extension;
                $enrolleeOrder->path_to_addition = $fullPath;
                $enrolleeOrder->save();
                if (!Storage::disk('public')->exists($fullPath)) {
                    Storage::disk('public')
                        ->put($fullPath, $fileContent);
                }
                usleep(300000);
            } catch (Throwable $exception) {
                Log::error(
                    "Не удалось получить фото додатка для person_request_id = " . $enrolleeOrder->person_request_id
                );
            }
        }
    }

    public function syncFaculties(): void
    {
        $response = $this->getAllCompetitiveOffers();

        foreach ($response as $value) {
            Faculty::updateOrCreate(
                [
                    'edbo_faculty_id' => $value['id_UniversityFacultet']
                ],
                [
                    'title' => $value['universityFacultetFullName']
                ]
            );
        }
    }

    protected function getAllCompetitiveOffers(): array
    {
        if (!isset($this->competitiveOffers) || is_null($this->competitiveOffers)) {
            $this->competitiveOffers = EDBOApiService::fetchCompetitiveOffers();
        }

        return $this->competitiveOffers;
    }

    public function syncQualificationLevels(): void
    {
        $response = $this->getAllCompetitiveOffers();

        foreach ($response as $value) {
            QualificationLevel::updateOrCreate(
                [
                    'edbo_qualification_level_id' => $value['qualificationGroupId']
                ],
                [
                    'title' => $value['qualificationGroupName']
                ]
            );
        }
    }

    public function syncSpecialties(): void
    {
        $response = $this->getAllCompetitiveOffers();

        foreach ($response as $value) {
            $faculty = Faculty::where('edbo_faculty_id', $value['id_UniversityFacultet'])->first();
            if (isset($value['specialityId'])) {
                Specialty::updateOrCreate(
                    [
                        'edbo_speciality_id' => $value['specialityId'],
                    ],
                    [
                        'title' => $value['specialityName'],
                        'faculty_id' => $faculty->id,
                        'code' => $value['specialityCode'],
                    ]
                );
            }
        }
    }

    public function syncSpecializations(): void
    {
        $response = $this->getAllCompetitiveOffers();

        foreach ($response as $value) {
            if ($value['specializationId']) {
                Specialization::updateOrCreate(
                    [
                        'edbo_specialization_id' => $value['specializationId']
                    ],
                    [
                        'title' => $value['specializationName'],
                        'code' => $value['specializationCode'],
                    ]
                );
            }
        }
    }

    public function syncCompetitiveOffers(): void
    {
        $response = $this->getAllCompetitiveOffers();

        foreach ($response as $value) {
            if ($value['isApplied']) {
                CompetitiveOffer::updateOrCreate(
                    [
                        'universitySpecialitiesId' => $value['universitySpecialitiesId']
                    ],
                    [
                        'universitySpecialitiesName' => $value['universitySpecialitiesName'],
                        'qualification_level_id' => QualificationLevel::where(
                            'title',
                            $value['qualificationGroupName']
                        )->first()->id,
                        'faculty_id' => Faculty::where('edbo_faculty_id', $value['id_UniversityFacultet'])->first()->id,
                        'speciality_id' => Specialty::where('edbo_speciality_id', $value['specialityId'])->first()->id,
                        'universitySpecialitiesTypeName' => $value['universitySpecialitiesTypeName'],
                        'educationFormName' => $value['educationFormName'],
                        'programNames' => $value['programNames'],
                    ]
                );
            }
        }
    }

    public function syncEnrolleeOrders(): void
    {
        $response = EDBOApiService::getAllCompetitiveOffersForMaster();
        foreach ($response as $competitiveOffer) {
            $orders = EDBOApiService::fetchOrders($competitiveOffer['universitySpecialitiesId']);

            if (count($orders) > 0) {
                foreach ($orders as $order) {
                    if ($order['personRequestStatusTypeId'] === 5) {
                        if ($order['isClaimForBudget'] && $order['isClaimForContract']) {
                            $result = 'budget_contract';
                        } elseif ($order['isClaimForBudget']) {
                            $result = 'budget';
                        } elseif ($order['isClaimForContract']) {
                            $result = 'contract';
                        }

                        EnrolleeOrder::query()
                            ->firstOrCreate(
                                [
                                    'person_id' => $order['personId'],
                                    'person_request_id' => $order['personRequestId'],
                                    'competitive_offer_id' => CompetitiveOffer::query()
                                        ->where(
                                            'universitySpecialitiesId',
                                            $competitiveOffer['universitySpecialitiesId']
                                        )->first()->id,
                                ],
                                [
                                    'full_name' => $order['fio'],
                                    'financing' => $result,
                                    'qualification_level_id' => QualificationLevel::query()
                                        ->where('title', $competitiveOffer['qualificationGroupName'])->first()->id,
                                    'status_id' => $order['personRequestStatusTypeId'],
                                    'status_id_in_edbo' => $order['personRequestStatusTypeId'],
                                    'documentSeries' => $order['documentSeries'],
                                    'documentNumber' => $order['documentNumbers'],
                                    'documentDateGet' => $order['documentDateGet'],
                                    'documentIssued' => $order['documentIssued'],
                                ]
                            );
                    }
                }
            }
        }
    }

    public function syncEnrolleeEnglishMarkZNO(): void
    {
        $response = EnrolleeOrder::all();

        foreach ($response as $enrolleeOrder) {
            $marks = [];

            try {
                $marks = EDBOApiService::post(
                    'api/entrance/personRequest/ZNOTechnology/list',
                    [
                        'personId' => $enrolleeOrder->person_id,
                    ]
                );
            } catch (Throwable $exception) {
                continue;
            }

            if (count($marks) === 1) {
                try {
                    $enrolleeOrder->english_mark = $marks[0]['subjectValue'];
                    $enrolleeOrder->certificate_zno = $marks[0]['documentNumbers'];
                    $enrolleeOrder->subjectYear = $marks[0]['subjectYear'];
                } catch (Throwable $exception) {
                    continue;
                }
                $enrolleeOrder->save();
            } elseif (count($marks) > 1) {
                foreach ($marks as $mark) {
                    if (isset($mark['subjectValue']) && $mark['subjectValue'] > 0.0) {
                        try {
                            $enrolleeOrder->english_mark = $mark['subjectValue'];
                            $enrolleeOrder->certificate_zno = $mark['documentNumbers'];
                            $enrolleeOrder->subjectYear = $mark['subjectYear'];
                            $enrolleeOrder->save();
                        } catch (Throwable $exception) {
                            continue;
                        }
                    }
                }
            }
        }
    }
}
