<?php

namespace App\Services\LOD;

use App\Models\EnrolleeOrder;
use Illuminate\Support\Facades\Log;

class LODSyncService
{
    public function syncDiplomaMarks(): void
    {
        $enrollees = EnrolleeOrder::all();
        foreach ($enrollees as $enrollee) {
            $response = LODApiService::post(
                'get_average_diplom_mark',
                [
                    'data' => [
                        "diploma_series" => $enrollee->documentSeries,
                        "diploma_number" => $enrollee->documentNumber
                    ]
                ]
            );

            if (isset($response['status']) && $response['status'] === 'success') {
                $enrollee->update(
                    [
                        'diploma_mark' => $response['diploma_mark'],
                        'average_addition_diploma_mark' => $response['average_mark']
                    ]
                );
            } else {
                Log::error("Error for get diploma marks for ".$enrollee->id);
            }
        }
    }
}
