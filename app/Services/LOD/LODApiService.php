<?php

namespace App\Services\LOD;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class LODApiService
{
    private Client $client;
    private string $url;

    private function __construct()
    {
        $this->client = new Client();
        $this->url = getenv('LOD_HOST');
    }

    public static function post(string $prefix = '', array $data = [], array $headers = []): array
    {
        $instance = new static();

        $payload = [
            'headers' => array_merge(
                [
                    'Content-Type' => 'application/json',
                    'Accept' => '*/*'
                ],
                $headers
            ),
            'json' => $data
        ];

        $response = null;

        try {
            $response = $instance->client->post($instance->url."/".$prefix, $payload);
        } catch (GuzzleException $exception) {
            Log::error("Ошибка при запросе в LOD: ".$exception->getMessage());
            return [];
        }

        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }
}
