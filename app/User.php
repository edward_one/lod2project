<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

/**
 * @property int id
 * @property string name
 * @property string email
 * @property int faculty_id
 * @property string password
 */
class User extends Authenticatable
{
    use Notifiable;

    public const TELEGRAM_CHAT_ID = -244225389;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasAdminPermission(): bool
    {
        return Str::endsWith($this->email, 'admin@ukr.net');
    }
}
