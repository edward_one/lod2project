<?php

namespace App\Nova\Filters;

use App\Models\CompetitiveOffer;
use App\User;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class ProgramNameFilter extends Filter
{
    public $name = "Фільтр по освітній програмі";

    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->whereHas('competitiveOffer', function($q) use ($value) {
            $q->where('programNames', $value);
        });
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        if ($user->hasAdminPermission()) {
            return CompetitiveOffer::select('programNames')
                ->distinct()
                ->get()
                ->pluck('programNames', 'programNames')
                ->toArray();
        }

        return CompetitiveOffer::select('programNames')
            ->where('faculty_id', $user->faculty_id)
            ->distinct()
            ->get()
            ->pluck('programNames', 'programNames')
            ->toArray();
    }
}
