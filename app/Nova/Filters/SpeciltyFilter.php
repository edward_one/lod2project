<?php

namespace App\Nova\Filters;

use App\Models\Specialty;
use App\User;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class SpeciltyFilter extends Filter
{
    public $name = "Фільтр по спеціальностям";

    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->whereHas('competitiveOffer', function($q) use ($value) {
            $q->where('speciality_id', $value);
        });

    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        if ($user->hasAdminPermission()) {
            return Specialty::all()->pluck('id', 'title')->toArray();
        }

        return Specialty::where('faculty_id', $user->faculty_id)->get()->pluck('id', 'title')->toArray();
    }
}
