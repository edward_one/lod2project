<?php

namespace App\Nova\Filters;

use App\Models\CompetitiveOffer;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class CompetitiveOfferFilter extends Filter
{
    public $name = " Фільтр по конкурсній пропозиції";

    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  Request  $request
     * @param  Builder  $query
     * @param  mixed  $value
     * @return Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('competitive_offer_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        if ($user->hasAdminPermission()) {
            $competitiveOffers = CompetitiveOffer::select(
                ['id', 'universitySpecialitiesName', 'universitySpecialitiesTypeName', 'educationFormName']
            )->orderBy('universitySpecialitiesName')->get()->map(function ($data) {
                return [$data['id'] => $data['universitySpecialitiesName'].'|'.$data['universitySpecialitiesTypeName'].'|'.$data['educationFormName']];
            });
        } else {
            $competitiveOffers = CompetitiveOffer::select(
                ['id', 'universitySpecialitiesName', 'universitySpecialitiesTypeName', 'educationFormName']
            )->orderBy('universitySpecialitiesName')->where('faculty_id', $user->faculty_id)->get()->map(function ($data) {
                return [$data['id'] => $data['universitySpecialitiesName'].'|'.$data['universitySpecialitiesTypeName'].'|'.$data['educationFormName']];
            });
        }

        $response = [];
        foreach ($competitiveOffers as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $response[$value1] = $key1;
            }
        }

        return $response;
    }
}
