<?php

namespace App\Nova\Dashboards;

use App\Nova\Metrics\EnrolleeOrdersStatisticByFacultyMetric;
use App\Nova\Metrics\EnrolleeOrdersStatisticMetric;
use Laravel\Nova\Dashboard;

class UserInsights extends Dashboard
{
    public function cards(): array
    {
        return [
            (new EnrolleeOrdersStatisticMetric)->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет інформаційних технологій та комп\'ютерної інженерії', 7))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет електроенергетики та електромеханіки', 6))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет комп`ютерних систем і автоматики', 5))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет машинобудування та транспорту', 1))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет менеджменту та інформаційної безпеки', 2))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет будівництва, теплоенергетики та газопостачання', 3))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Факультет інфокомунікацій, радіоелектроніки та наносистем', 4))->width('full'),
            (new EnrolleeOrdersStatisticByFacultyMetric('Інститут екологічної безпеки та моніторингу довкілля', 8))->width('full'),
        ];
    }

    public static function label(): string
    {
        return 'Статистика по статусам';
    }

    public static function uriKey(): string
    {
        return 'user-insights';
    }
}
