<?php

namespace App\Nova;

use App\Enums\EnrolleeEducationBudget;
use App\Enums\EnrolleeFinancing;
use App\Enums\EnrolleeOrderStatuses;
use App\Nova\Filters\CompetitiveOfferFilter;
use App\Nova\Filters\ProgramNameFilter;
use App\Nova\Filters\SpeciltyFilter;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;
use Pdewit\ExternalUrl\ExternalUrl;

class EnrolleeOrder extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\EnrolleeOrder::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'full_name';

    public static $trafficCop = false;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'full_name'
    ];

    public static $group = 'Вступна кампанія';

    public static $showColumnBorders = true;

    public static $debounce = 0.5; // 0.5 seconds

    public static function label()
    {
        return "Заява абітурієнта";
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        $user = $request->user();
        if ($user->hasAdminPermission()) {
            return $query;
        }
        return $query->whereHas(
            'competitiveOffer',
            function ($q) use ($user) {
                $q->where('faculty_id', $user->faculty_id);
            }
        );
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->hide()
                ->sortable(),

            ExternalUrl::make(
                'Згенерувати запрошення',
                function () {
                    return '/invite/'.$this->id;
                }
            ),

            Avatar::make("Фото", 'path_to_photo')->maxWidth(150)->squared(),

            File::make("Скан додатку до диплома", 'path_to_addition')->disk('public'),

            File::make("Заява", 'order_file_path')->disk('public'),

            Text::make("Номер особової справи", 'personal_file_number')
                ->help('Номер особової справи (наприклад: 101-О)')
                ->required(),

            Text::make("ПІБ", 'full_name')
                ->required()
                ->readonly(
                    function ($request) {
                        return $request->isUpdateOrUpdateAttachedRequest();
                    }
                ),

            Select::make("Фінансування", 'financing')
                ->options(EnrolleeFinancing::CONFIG)
                ->default('budget_contract')
                ->displayUsing(static fn($name) => EnrolleeFinancing::CONFIG[$name])
                ->readonly()
                ->required(),

            BelongsTo::make("Конкурсна пропозиція", 'competitiveOffer', 'App\Nova\CompetitiveOffer')
                ->required()
                ->readonly(
                    function ($request) {
                        return $request->isUpdateOrUpdateAttachedRequest();
                    }
                ),

            BelongsTo::make("Освітній ступінь", 'qualificationLevel', 'App\Nova\QualificationLevel')
                ->default(2)
                ->required()
                ->hideFromIndex()
                ->readonly(
                    function ($request) {
                        return $request->isUpdateOrUpdateAttachedRequest();
                    }
                ),

            Boolean::make("Подано оригінал документів", 'isOriginalDocumentsAdded')
                ->default(false)
                ->required(),

            Boolean::make("Подано довідку про місцезнаходження документів", 'informationOriginalDocumentLocation')
                ->default(false)
                ->required(),

            Select::make("Освітній ступінь за кошти державного або місцевого бюджету", 'education_budget')
                ->options(EnrolleeEducationBudget::CONFIG)
                ->displayUsing(static fn($name) => EnrolleeEducationBudget::CONFIG[$name])
                ->default(1)
                ->required()
                ->hideFromIndex(),

            Select::make("Статус заяви", 'status_id')
                ->options(EnrolleeOrderStatuses::CONFIG)
                ->displayUsing(static fn($name) => EnrolleeOrderStatuses::CONFIG[$name])
                ->default('5')
                ->required(),

            BelongsTo::make( 'Наказ', 'order', Order::class)->nullable(),

            Boolean::make("Готово до синхронізації", 'is_ready_for_sync')
                ->required(),

            new Panel('Результати вступних випробувань та конкурсних показників', $this->getMarkFields()),
        ];
    }

    public function getMarkFields(): array
    {
        return [
            Boolean::make("Потрібно пройти співбесіду", 'is_need_interview')
                ->default(false)
                ->required(),

            NovaDependencyContainer::make(
                [
                    Number::make("Оцінка за співбесіду", 'interview_mark')
                        ->rules('required'),
                ]
            )->hideFromIndex()->dependsOn('is_need_interview', 1)->rules('required'),

            Number::make('Оцінка за ЗНО з англійської мови', 'english_mark')
                ->default(0)
                ->showOnDetail(
                    function () {
                        return !($this->certificate_zno === '' || is_null($this->certificate_zno));
                    }
                )
                ->showOnUpdating(
                    function () {
                        return !($this->certificate_zno === '' || is_null($this->certificate_zno));
                    }
                )
                ->readonly(),
            Number::make('Середній бал додатка', 'average_addition_diploma_mark')
                ->default(0)
                ->readonly(),
            Number::make('Захист диплому', 'diploma_mark')
                ->default(0)
                ->readonly(),
            Number::make('Вступне фахове випробування', 'speciality_mark')
                ->default(0),
            Text::make('Додаткові бали', 'additional_mark')
                ->help('Писати через крапку, приклад - 197.1')
                ->default(0),

            Number::make('Оцінка за іспит з англійської мови (в університеті)', 'english_mark_from_university')
                ->showOnDetail(
                    function () {
                        return $this->certificate_zno === '' || is_null($this->certificate_zno);
                    }
                )
                ->showOnUpdating(
                    function () {
                        return $this->certificate_zno === '' || is_null($this->certificate_zno);
                    }
                )
                ->default(0),

            Text::make('Конкурсний бал', 'rating')
                ->sortable()
                ->default(0)
                ->readonly(),
        ];
    }

    public function fieldsForIndex(): array
    {
        return [
            Avatar::make("Фото", 'path_to_photo')->squared(),

            Text::make("Номер особової справи", 'personal_file_number')
                ->help('Номер особової справи (наприклад: 101-О)')
                ->required(),

            Text::make(
                "ПІБ",
                function () {
                    return '<a href="/nova/resources/enrollee-orders/'.$this->id.'" class="no-underline dim text-primary font-bold" target="_blank">'.$this->full_name.'</a>';
                }
            )->asHtml()
                ->readonly(
                    function ($request) {
                        return $request->isUpdateOrUpdateAttachedRequest();
                    }
                )
                ->required(),

            Select::make("Фінансування", 'financing')
                ->options(EnrolleeFinancing::CONFIG)
                ->default('budget_contract')
                ->displayUsing(static fn($name) => EnrolleeFinancing::CONFIG[$name])
                ->required(),

            BelongsTo::make("Конкурсна пропозиція", 'competitiveOffer', 'App\Nova\CompetitiveOffer')
                ->required()
                ->readonly(
                    function ($request) {
                        return $request->isUpdateOrUpdateAttachedRequest();
                    }
                ),

            BelongsTo::make("Освітній ступінь", 'qualificationLevel', 'App\Nova\QualificationLevel')
                ->default(2)
                ->required()
                ->hideFromIndex()
                ->readonly(
                    function ($request) {
                        return $request->isUpdateOrUpdateAttachedRequest();
                    }
                ),

            Boolean::make("Подано оригінал документів", 'isOriginalDocumentsAdded')
                ->default(false)
                ->required(),

            Boolean::make("Подано довідку про місцезнаходження документів", 'informationOriginalDocumentLocation')
                ->default(false)
                ->required(),

            Select::make("Освітній ступінь за кошти державного або місцевого бюджету", 'education_budget')
                ->options(EnrolleeEducationBudget::CONFIG)
                ->displayUsing(static fn($name) => EnrolleeEducationBudget::CONFIG[$name])
                ->default(1)
                ->required()
                ->hideFromIndex(),

            Select::make("Статус заяви", 'status_id')
                ->options(EnrolleeOrderStatuses::CONFIG)
                ->displayUsing(static fn($name) => EnrolleeOrderStatuses::CONFIG[$name])
                ->default('5')
                ->required(),

            Boolean::make("Готово до синхронізації", 'is_ready_for_sync')
                ->required(),

            new Panel('Результати вступних випробувань та конкурсних показників', $this->getMarkFields()),

            DateTime::make('Час створення заяви', 'created_at')
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new SpeciltyFilter(),
            new ProgramNameFilter(),
            new CompetitiveOfferFilter()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function relatableOrders(NovaRequest $request, $query)
    {
        return $query->where('qualificationGroupName', 'Магістр');
    }
}
