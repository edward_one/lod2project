<?php

namespace App\Nova\Metrics;

use App\Enums\EnrolleeOrderStatuses;
use App\Models\EnrolleeOrder;
use DateInterval;
use DateTimeInterface;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Partition;

class EnrolleeOrdersStatisticMetric extends Partition
{
    /**
     * Calculate the value of the metric.
     *
     * @param  NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
        return $this->count($request, EnrolleeOrder::class, 'status_id')
            ->label(
                function ($value) {
                    return EnrolleeOrderStatuses::CONFIG[$value];
                }
            );
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  DateTimeInterface|DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    public function uriKey(): string
    {
        return 'enrollee-orders-statistic-metric';
    }

    public function name(): string
    {
        return 'Статуси заяв';
    }
}
