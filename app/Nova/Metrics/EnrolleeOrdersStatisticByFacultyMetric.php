<?php

namespace App\Nova\Metrics;

use App\Enums\EnrolleeOrderStatuses;
use App\Models\EnrolleeOrder;
use DateInterval;
use DateTimeInterface;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Partition;

class EnrolleeOrdersStatisticByFacultyMetric extends Partition
{
    protected string $translateName;
    protected int $facultyId;

    public function __construct($translateName, $facultyId, $component = null)
    {
        parent::__construct($component);
        $this->translateName = $translateName;
        $this->facultyId = $facultyId;
    }

    /**
     * Calculate the value of the metric.
     *
     * @param  NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
        return $this->count(
            $request,
            EnrolleeOrder::whereHas(
                'competitiveOffer',
                function ($q) {
                    return $q->where('faculty_id', $this->facultyId);
                }
            ),
            'status_id'
        )
            ->label(
                function ($value) {
                    return EnrolleeOrderStatuses::CONFIG[$value];
                }
            );
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  DateTimeInterface|DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'enrollee-orders-statistic-by-faculty-metric-'.$this->facultyId;
    }

    public function name(): string
    {
        return $this->translateName;
    }
}
