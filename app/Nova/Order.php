<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class Order extends Resource
{
    public static $model = \App\Models\Order::class;

    public static $displayInNavigation = false;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'orderOfEnrollmentNumber',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Номер наказу', 'orderOfEnrollmentNumber')->readonly(),
            Text::make('Дата наказу', 'orderOfEnrollmentDate')->readonly(),
            Text::make('Освітній ступінь (ОКР)', 'qualificationGroupName')->readonly(),
            Text::make('Вступ на основі', 'educationBaseName')->readonly(),
            Text::make('Форма навчання', 'educationFormName')->readonly(),
            Text::make('Курс', 'courseName')->readonly(),
            Text::make('Джерело фінансування', 'personEducationPaymentTypeName')->readonly(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public function title()
    {
        return $this->orderOfEnrollmentNumber . '('. $this->personEducationPaymentTypeName . ')' . ' (' . $this->educationFormName . ')';
    }
}
