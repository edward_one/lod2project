<?php

namespace App\Policies;

use App\Models\EnrolleeOrder;
use App\Models\Order;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function view(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function create(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function update(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function delete(User $user): bool
    {
        return $user->hasAdminPermission();
    }
}
