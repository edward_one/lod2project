<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function delete(User $user): bool
    {
        return $user->hasAdminPermission();
    }
}
