<?php

namespace App\Policies;

use App\Models\EnrolleeOrder;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnrolleeOrderPolicy
{
    use HandlesAuthorization;

    public function create(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function view(User $user, EnrolleeOrder $enrolleeOrder): bool
    {
        return $user->hasAdminPermission() || $user->faculty_id === $enrolleeOrder->competitiveOffer->faculty_id;
    }

    public function update(User $user, EnrolleeOrder $enrolleeOrder): bool
    {
        return $user->hasAdminPermission() || $user->faculty_id === $enrolleeOrder->competitiveOffer->faculty_id;
    }

    public function delete(User $user, EnrolleeOrder $enrolleeOrder): bool
    {
        return $user->hasAdminPermission();
    }
}
