<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompetitiveOfferPolicy
{
    use HandlesAuthorization;

    public function view(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function delete(User $user): bool
    {
        return $user->hasAdminPermission();
    }

    public function update(User $user): bool
    {
        return $user->hasAdminPermission();
    }
}
