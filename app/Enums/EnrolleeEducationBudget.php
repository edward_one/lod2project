<?php

namespace App\Enums;

class EnrolleeEducationBudget
{
    public const CONFIG = [
        '1' => 'Ніколи не здобувався',
        '2' => 'Вже здобутий раніше',
        '3' => 'Вже здобувався раніше (навчання не завершено)',
    ];
}
