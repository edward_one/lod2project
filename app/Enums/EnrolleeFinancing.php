<?php

namespace App\Enums;

class EnrolleeFinancing
{
    public const CONFIG = [
        'budget' => 'Бюджет',
        'contract' => 'Контракт',
        'budget_contract' => 'Бюджет та контракт',
        'additional' => 'Цільове',
    ];
}
