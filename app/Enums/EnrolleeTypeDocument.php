<?php

namespace App\Enums;

class EnrolleeTypeDocument
{
    public const CONFIG = [
        'original' => 'Оригінал',
        'copy' => 'Довідка',
    ];
}
