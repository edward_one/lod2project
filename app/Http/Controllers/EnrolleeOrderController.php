<?php

namespace App\Http\Controllers;

use App\Models\EnrolleeOrder;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class EnrolleeOrderController extends Controller
{
    public function generateInvite(Request $request, $id)
    {
        $enrollee = EnrolleeOrder::with('competitiveOffer.faculty')
            ->findOrFail($id);

        /**
         * @var User $user
         */
        $user = \Auth::user();

        if($user->hasAdminPermission() || $user->faculty_id === $enrollee->competitiveOffer->faculty_id) {
            $opciones_ssl=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $path_img = '';
            if($enrollee->path_to_photo) {
                $extension = pathinfo($enrollee->path_to_photo, PATHINFO_EXTENSION);
                $data = file_get_contents('storage/'.$enrollee->path_to_photo, false, stream_context_create($opciones_ssl));
                $img_base_64 = base64_encode($data);
                $path_img = 'data:image/' . $extension . ';base64,' . $img_base_64;
            }

            $pdf = PDF::setOptions(['isPhpEnabled' => true])->loadView(
                'invite',
                [
                    'enrollee' => $enrollee,
                    'image' => $path_img
                ]
            )->setPaper('a4', 'landscape');

            return $pdf->stream('invoice.pdf');
        } else {
            return response('Forbidden', 403);
        }
    }
}
