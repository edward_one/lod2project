<?php

namespace App\Http\Controllers;

use App\Jobs\ImportDiplomsForEnrolleeOrdersJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CSVImporterController extends Controller
{
    public function importFile(Request $request): JsonResponse
    {
        $fileName = $this->uploadCSV(
            $request->file('csv_file'),
            'csv'
        );

        ImportDiplomsForEnrolleeOrdersJob::dispatch($fileName);

        return response()->json(['status' => 'success']);
    }

    public function uploadCSV(UploadedFile $file, string $dir): string
    {
        if (!$this->fileValidation($file)) {
            return '';
        }

        $filename = $this->generateUniqueFileName();
        $extension = $file->getClientOriginalExtension();

        $fullName = "$filename.$extension";
        $file->storeAs(
            $dir,
            $fullName,
            ['disk' => 'public']
        );

        return $fullName;
    }

    public function fileValidation(UploadedFile $file): bool
    {
        return $file && UPLOAD_ERR_OK === $file->getError() && $file->isFile();
    }

    protected function generateUniqueFileName(): string
    {
        return Str::random(40);
    }

    public function readCSV($csvFile, $array): array
    {
        $file_handle = fopen(storage_path($csvFile), 'r');
        $line_of_text = [];
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }
}
