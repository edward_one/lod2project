<?php

namespace App\Console\Commands;

use App\Models\EnrolleeOrder;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Console\Command;

class AttachOrderFromEDBOToEnrolleeOrderCommand extends Command
{
    protected $signature = 'attach:orders';

    protected $description = 'Command description';

    public function handle(): int
    {
        $enrolls = EnrolleeOrder::query()->whereNull('order_file_path')->get();

        $bar = $this->output->createProgressBar($enrolls->count());

        $bar->start();

        foreach ($enrolls as $enrollee) {
            EDBOSyncService::syncEDBOOrderForEnrollee($enrollee);

            $bar->advance();
        }

        $bar->finish();

        $this->line('Finish!');

        return 0;
    }
}
