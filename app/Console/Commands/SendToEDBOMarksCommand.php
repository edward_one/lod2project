<?php

namespace App\Console\Commands;

use App\Models\EnrolleeOrder;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Console\Command;

class SendToEDBOMarksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:marks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $enrolls = EnrolleeOrder::query()
        ->get();

        $bar = $this->output->createProgressBar($enrolls->count());

        $bar->start();

        foreach ($enrolls as $enrollee) {
            EDBOSyncService::sendMarksToEDBOForEnrollee($enrollee);

            $bar->advance();
        }

        $bar->finish();

        $this->line('Finish!');

        return 0;
    }
}
