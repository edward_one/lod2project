<?php

namespace App\Console\Commands;

use App\Models\EnrolleeOrder;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SendToEDBOStatusesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:statuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $enrolls = EnrolleeOrder::query()
            ->where('status_id', 6)
//            ->whereHas('competitiveOffer', function (Builder $q) {
//                $q->where('faculty_id', 1)
//                ->orWhere('faculty_id', 2)
//                ->orWhere('faculty_id', 7);
//            })
            ->where('is_ready_for_sync', true)
            ->get();

        $bar = $this->output->createProgressBar($enrolls->count());

        $bar->start();

        foreach ($enrolls as $enrollee) {
            EDBOSyncService::updateStatusForEnrollee($enrollee);

            $bar->advance();
        }

        $bar->finish();

        $this->line('Finish!');

        return 0;
    }
}
