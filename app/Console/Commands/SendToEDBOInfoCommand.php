<?php

namespace App\Console\Commands;

use App\Models\EnrolleeOrder;
use App\Services\EDBO\EDBOSyncService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SendToEDBOInfoCommand extends Command
{
    protected $signature = 'send:info';

    protected $description = 'Command description';

    public function handle(): int
    {
        $enrolls = EnrolleeOrder::query()
            ->get();

        $bar = $this->output->createProgressBar($enrolls->count());

        $bar->start();

        foreach ($enrolls as $enrollee) {
            EDBOSyncService::sendInfoForEnrollee($enrollee);

            $bar->advance();
        }

        $bar->finish();

        $this->line('Finish!');

        return 0;
    }
}
