<?php

namespace App\Providers;

use App\Models\CompetitiveOffer;
use App\Models\EnrolleeOrder;
use App\Models\Faculty;
use App\Models\Order;
use App\Models\QualificationLevel;
use App\Policies\CompetitiveOfferPolicy;
use App\Policies\EnrolleeOrderPolicy;
use App\Policies\FacultyPolicy;
use App\Policies\OrderPolicy;
use App\Policies\QualificationLevelPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        EnrolleeOrder::class => EnrolleeOrderPolicy::class,
        CompetitiveOffer::class => CompetitiveOfferPolicy::class,
        QualificationLevel::class => QualificationLevelPolicy::class,
        User::class => UserPolicy::class,
        Faculty::class => FacultyPolicy::class,
        Order::class => OrderPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}
