<?php

namespace App\Providers;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\ServiceProvider;
use NotificationChannels\Telegram\Telegram;
use NotificationChannels\Telegram\TelegramChannel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Notification::resolved(
            function (ChannelManager $service) {
                $service->extend(
                    'telegram',
                    function ($app) {
                        return new TelegramChannel(
                            new Telegram(
                                config('services.telegram-bot-api.token'),
                                app(HttpClient::class),
                                config('services.telegram-bot-api.base_uri')
                            )
                        );
                    }
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
